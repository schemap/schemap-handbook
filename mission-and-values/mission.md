# Mission Statement
 
## "Making informed decisions easy"

All data driven organizations should focus on inferring the insights hidden in their data. Emerging trends deciphered can put a company in better position to gain a dominant position in a market. We envision a future where companies can setup their data analytics infrastructure with minimal effort. 

> “Civilization advances by extending the number of important operations which we can perform without 
> thinking of them."   
> [Alfred Whitehead]
