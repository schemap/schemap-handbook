
# SchemaP Values

> "The CEO Is Not In Charge Of The Company. The Values Are."  **( Dave Logan )**

We believe that people drive the company forward. People make and break companies. SchemaP values revolve around its people. Company culture emerges by the values held and followed by individuals
who make up the company. In absence of empowerment, one cannot expect an individual to be effective, responsible, learn from mistakes and grow. Our core values revolve around autonomy, freedom and continous improvement for the people who work with us, and for the people we serve.

## Honesty
To be truthful and upright at all times. We must always adhere to facts instead of perceived ideas, biases or any thought that hinders or runs counter to unequivocal truth.

## Empowerment
To strive for the increase of the degree of autonomy, and self determination in individuals and groups.

## Effectiveness
To become successful in delivering the desired result.

## Responsibility
To live with a conscious mind. Everyone can make decisions that not only effect them but also the people around them. All such thoughts and actions must be with pure intention to improve and help one's self and others. We believe your self is a separate entity that also has rights on you.

## Continous Improvement
To strive for continous improvement not only in skill but also in ethics, manners, thoughts
and personal conduct.

## Sincerity Not Loyalty
Like individuals companies also go through tough times. In good or bad times we must be sincere in our conduct. We do not believe in loyality as it brings some negative connotations to mind. If you find better opportunities then you must think about your future and your loved ones first and take better offers.
