# Schema Processing Handbook

We believe in transparency. Therefore, our handbook is a git repository rather than a collection of papers. Anyone can see the princples on which we function, the beliefs that we value and how our ideals shape our vision of what the future must be. You can communicate your own ideas and suggestions by simply creating a ticket. The ticket should state your intent and purpose. Your ideas can be collaboratively discussed. Changing a policy is a matter of discourse, understanding, practicality and a merge.

Improvements in company culture, processes and policies is a continous process. We are open
to all voices belonging to different school of thoughts. If you find any way in which the culture of Schema Processing can be improved then feel free to share your thoughts.

Wasim Zarin


# Introduction
- [Mission](mission-and-values/mission.md)
- [Values](mission-and-values/values.md)

# Employment Policies
- [At-Will Employment](employment-policies/at-will-employment.md)
- [Equal Employment Opportunity](employment-policies/equal-employment-opportunity.md)
- [Code Of Conduct](employment-policies/code-of-conduct.md)
- [Communication Guidelines](employment-policies/communication-guidelines.md)
- [Confidentiality](employment-policies/confidentiality.md)
- [Harassment](employment-policies/harassment.md)
- [Dress Code](employment-policies/dress-code.md)
- [Intoxicant And Weapon Policy](employment-policies/intoxicant-weapon.md)
- [Reporting A Violation](employment-policies/reporting-violation.md)
- [Employee Privacy](employment-policies/employee-privacy.md)
- [Taking Break During Work](employment-policies/breaks.md)
- [Time Tracking](employment-policies/time-tracking.md)
- [Salary](employment-policies/salary.md)
- [Counselling](employment-policies/mental-health.md)
