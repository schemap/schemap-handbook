# Code Of Conduct

Everyone who is part of SchemaP represents us. Therefore, it is incumbent upon all of us to hold ourselves to highest epitome of noble character and ethical integrity. The guidelines mentioned here
are binding on all people who belong to SchemaP in any capacity whether they are directors, executives, managers or employees.

These guidelines must be adhered in all situations of personal conduct. Some of them(not all) are:
- On workplace
- Any meetings or conferences whether in person or online
- Any correspondences with people inside or outside of SchemaP
- Social media where your statments and actions can be seen by many