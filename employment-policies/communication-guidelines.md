# Guidelines for Communicating With Others

When you communicate with others irrespective of the medium of communication being used:

- Always politely support or identify the mistakes of others. Avoid angry tone
- Respect beliefs, values and thoughts of others
- Bullying is intolerable. Avoid rude or commanding tone
- Never ever make a sexist, racist, obscene or offensive remark or gesture to anyone
