# Equal Employment Opportunity
SchemaP is an Equal Opportunity Employer. SchemaP does not discriminate against qualified employees or
applicants based on gender, religion, caste, ethnicity, color, age, disability(physical,mental or sensory) or any other characteristic protected by federal, provincal or local laws or ordinances.
