# Intoxicant And Weapon Policy

Remember a safe and comfortable working environment is top priority for us. Not following such rules can result in termination. To make sure that no one feels uncomfortable or intimidated one is bound to 
adhere to following injunctions. Failing to do so can result in termination.

Bringing intoxicants to work or working under influence is not allowed. This may cause discomfort among your peers. Intoxicants include all intoxicating substances. This can be any thing that can cause intoxicating effects. Moreover selling, distributing or consuming intoxicants during work or at workplace is is not allowed.

Weapons like firearms, knives, explosives or any explodable substances or combustable artifacts like fireworks that may result in arson or explosion are not allowed on workplace under any circumstances. You are not allowed to bring any sort of things that can be classified as weapons or can be lethaly used against someone. Safety at workplace is our utmost priority.