# Dress Code

We have a relaxed dress code policy. Wear whatever you find comfortable. Casual dresses like t-shirts, jeans, athletic wear and skirts(please read ahead to determine what sort of dresses are acceptable) are all acceptable ways of dressing. However one should not dress in an unprofessional manner or extragavantly distracting manner. Please avoid clothing that is revealing, distracting and untidy. Also keep in mind the norms and cultures of the social setting when choosing what to wear.
