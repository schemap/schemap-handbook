# Employee Privacy

To ensure a healthy and secure working environment SchemaP may need to track certain
activities of their employees. The purpose of this is by no means surveillance. We want people
at SchemaP to be comfortable. However, to make sure that we create a safe working environment
certain kind of observations are necessary. Your workstations and company property (e.g laptops) are 
subject to search.

We respect privacy of SchemaP employees. However, the workspaces and assets belonging to SchemaP can
be searched by the company without any prior warning. The reason for this provision is that assets and
workspace is ownership of SchemaP. This measure is necessary to investigate any violations of SchemaP
policy like harassment, theft, drugs or possession of any items deemed illegal under federal, provincal
or local laws.

## SchemaP Can Monitor Your Emails
We do not monitor emails of our employees. However, it is a company resource for official purposes. 
Email messages, including attachments, sent and received from a SchemaP email address are the property of SchemaP. SchemaP has the right to access, monitor, read, and/or copy email messages at any time, for any reason. The company has the right to look into use of company emails to make sure that there is no breach or violation of any SchemaP policies. Your company email is strictly for official purposes. Therefore, use your official email carefully. If any use of company email violates any policy of SchemaP or hinders your job performance and duty then it will be considered violation of usage of company resources.


## Securing Email Is Your Responsibility
- Avoid spam, phishing or emails from unknown senders.
- Be careful in opening email attachments. Do not open attachments from unknown senders.
- If your computer system is infected or any of the services you use for offical purposes have been 
compromised then inform the CEO immediately.
- Sharing your credentials or letting anyone else use your identity is prohibbitted. Do not share any 
such information with anyone including your co-workers or family members. 

## Compliance With SchemaP Policies

All of our policies and rules of conduct apply to employee use of the email system. This means, for example, that you may not use any resources to send harassing or discriminatory messages, including messages with explicit sexual content or pornographic images; to send threatening messages; or to reveal company trade secrets.

## Careful On Social Media
Be professional and polite when communicating with others. This also holds when you share posts on your
social media accounts, in your social conversations, or when you post as or on SchemaP online or social
media accounts.

## All Conduct Rules Apply to Email
All of our policies and rules of conduct apply to employee use of the email system. This means, for example, that you may not use the email system to send harassing or discriminatory messages, including messages with explicit sexual content or pornographic images; to send threatening messages; or to reveal company trade secrets.

## Company Internet Use Is Not Private
We reserve the right to monitor employee use of the Internet at any time. Propagating, surfing, sharing or accessing obscene, illegal or pornographic material is prohibitted.