# Breaks During Work
We encourage you to take lunch breaks during the day. We follow an eight hour work day schedule for 5 days a week (if not stated otherwise in your offer letter). We expect you to take a lunch break lasting maximum to around an hour. Following are some guidelines on breaks:
- Eat at the designated place
- You can also go to a nearby resturant for a meal
- It is encouraged to have lunch together with your colleagues to know them better
